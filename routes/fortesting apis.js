const { default: mongoose } = require("mongoose");
const db = require("../db");
let User = db.User
let Roles = db.Roles
let TrustPoints = db.TrustPoints
let ServiceProvider = db.ServiceProvider
let ServicesSP = db.ServicesSP
let Category = db.Category
let Skills = db.Skills
let SubCategory = db.SubCategory
const postmanaddCategory = async (req, res) => {

    try {
        // const categoriesData = [
        //     {
        //         title: "PHP",
        //         tagLine: "Server-side scripting language",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         topcategory: true,
        //         popular_category: true,
        //         description1: "PHP is a popular general-purpose scripting language that is especially suited to web development.",
        //         description2: "Fast, flexible and pragmatic, PHP powers everything from your blog to the most popular websites in the world.",
        //         status: "Active"
        //     },
        //     {
        //         title: "WordPress",
        //         tagLine: "Content Management System",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         topcategory: true,
        //         popular_category: true,
        //         description1: "WordPress is a free and open-source content management system written in PHP and paired with a MySQL or MariaDB database.",
        //         description2: "Features include a plugin architecture and a template system, referred to within WordPress as Themes.",
        //         status: "Active"
        //     },
        //     {
        //         title: "JavaScript",
        //         tagLine: "Client-side scripting language",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         topcategory: true,
        //         popular_category: true,
        //         description1: "JavaScript is a lightweight, interpreted, or just-in-time compiled programming language with first-class functions.",
        //         description2: "While it is most well-known as the scripting language for web pages, many non-browser environments also use it.",
        //         status: "Active"
        //     }, {
        //         title: "Python",
        //         tagLine: "High-level programming language",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         topcategory: true,
        //         popular_category: true,
        //         description1: "Python is an interpreted, high-level, general-purpose programming language.",
        //         description2: "Python's design philosophy emphasizes code readability with its notable use of significant indentation.",
        //         status: "Active"
        //     },
        //     {
        //         title: "Java",
        //         tagLine: "General-purpose programming language",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         topcategory: true,
        //         popular_category: false,
        //         description1: "Java is a high-level, class-based, object-oriented programming language.",
        //         description2: "Java is designed to have as few implementation dependencies as possible, allowing developers to write once, run anywhere.",
        //         status: "Active"
        //     },
        //     {
        //         title: "React",
        //         tagLine: "JavaScript library for building user interfaces",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         topcategory: true,
        //         popular_category: false,
        //         description1: "React is an open-source, front-end, JavaScript library for building user interfaces or UI components.",
        //         description2: "React is maintained by Facebook and a community of individual developers and companies.",
        //         status: "Active"
        //     },{
        //         title: "Angular",
        //         tagLine: "Platform for building mobile and desktop web applications",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         topcategory: true,
        //         popular_category: false,
        //         description1: "Angular is a platform and framework for building single-page client applications using HTML and TypeScript.",
        //         description2: "Angular is maintained by Google and a community of individual developers and corporations.",
        //         status: "Active"
        //     },
        //     {
        //         title: "Node.js",
        //         tagLine: "JavaScript runtime built on Chrome's V8 JavaScript engine",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         topcategory: true,

        //         description1: "Node.js is an open-source, cross-platform JavaScript runtime environment that executes JavaScript code outside of a web browser.",
        //         description2: "Node.js is designed to build scalable network applications and is commonly used for building web servers.",
        //         status: "Active"
        //     },
        //     {
        //         title: "Vue.js",
        //         tagLine: "Progressive JavaScript framework",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         description1: "Vue.js is an open-source JavaScript framework for building user interfaces and single-page applications.",
        //         description2: "Vue.js is designed to be incrementally adoptable and can also function as a web application's core library.",
        //         status: "Active"
        //     },{
        //         title: "React Native",
        //         tagLine: "Build native mobile apps using React",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         description1: "React Native is an open-source framework for building mobile applications using React and native platform capabilities.",
        //         description2: "With React Native, you can develop applications for both iOS and Android platforms using a single codebase.",
        //         status: "Active"
        //     },
        //     {
        //         title: "Django",
        //         tagLine: "High-level Python web framework",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         description1: "Django is a high-level Python web framework that encourages rapid development and clean, pragmatic design.",
        //         description2: "Django follows the Model-View-Template (MVT) architectural pattern and includes many built-in features for web development.",
        //         status: "Active"
        //     },
        //     {
        //         title: "Ruby on Rails",
        //         tagLine: "Web application framework written in Ruby",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         description1: "Ruby on Rails, or Rails, is a server-side web application framework written in Ruby under the MIT License.",
        //         description2: "Rails is a model–view–controller (MVC) framework, providing default structures for a database, a web service, and web pages.",
        //         status: "Active"
        //     },
        //     {
        //         title: "Laravel",
        //         tagLine: "PHP framework for web artisans",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         description1: "Laravel is a free, open-source PHP web framework, intended for the development of web applications following the model–view–controller (MVC) architectural pattern.",
        //         description2: "Laravel aims to make the development process a pleasing one for the developer without sacrificing application functionality.",
        //         status: "Active"
        //     },
        //     {
        //         title: "Java Spring",
        //         tagLine: "Application framework and inversion of control container for Java",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         description1: "Spring Framework is an application framework and inversion of control container for the Java platform.",
        //         description2: "Spring provides comprehensive infrastructure support for developing Java applications and is widely used for building enterprise-level applications.",
        //         status: "Active"
        //     },
        //     {
        //         title: "Vue.js",
        //         tagLine: "Progressive JavaScript framework",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         description1: "Vue.js is an open-source JavaScript framework used for building user interfaces and single-page applications.",
        //         description2: "Vue.js is designed to be incrementally adoptable, making it suitable for integrating into existing projects.",
        //         status: "Active"
        //     },
        //     {
        //         title: "Angular",
        //         tagLine: "Platform for building mobile and desktop web applications",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         description1: "Angular is a platform and framework for building single-page client applications using HTML and TypeScript.",
        //         description2: "Angular provides a comprehensive solution that includes tools for building applications with declarative templates, dependency injection, and end-to-end tooling.",
        //         status: "Active"
        //     },
        //     {
        //         title: "Node.js",
        //         tagLine: "JavaScript runtime built on Chrome's V8 JavaScript engine",
        //         icon: "nodejs-icon.png",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         description1: "Node.js is an open-source, cross-platform JavaScript runtime environment that executes JavaScript code outside of a browser.",
        //         description2: "Node.js is commonly used for building server-side and networking applications, and it allows developers to use JavaScript for both client-side and server-side scripting.",
        //         status: "Active"
        //     },
        //     {
        //         title: "Express.js",
        //         tagLine: "Fast, unopinionated, minimalist web framework for Node.js",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         description1: "Express.js is a web application framework for Node.js, designed for building web applications and APIs.",
        //         description2: "Express.js provides a thin layer of fundamental web application features, allowing developers to build robust web applications quickly and easily.",
        //         status: "Active"
        //     },
        //     {
        //         title: "MongoDB",
        //         tagLine: "Document-oriented NoSQL database",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         description1: "MongoDB is a cross-platform document-oriented NoSQL database program. Classified as a NoSQL database program, MongoDB uses JSON-like documents with optional schemas.",
        //         description2: "MongoDB is developed by MongoDB Inc. and licensed under the Server Side Public License (SSPL).",
        //         status: "Active"
        //     },
        //     {
        //         title: "React Native 0.2",
        //         tagLine: "Build native mobile apps using React",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         description1: "React Native is a framework for building native applications using React. It allows you to write mobile applications using JavaScript and React.",
        //         description2: "With React Native, you can develop applications for both iOS and Android platforms using a single codebase, reducing development time and effort.",
        //         status: "Active"
        //     },
        //     {
        //         title: "Django 1",
        //         tagLine: "High-level Python web framework",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         description1: "Django is a high-level Python web framework that encourages rapid development and clean, pragmatic design.",
        //         description2: "Django follows the model-template-view (MTV) architectural pattern and emphasizes reusability and pluggability of components.",
        //         status: "Active"
        //     },
        //     {
        //         title: "Ruby on Rails 0.2",
        //         tagLine: "Web application framework for Ruby",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         description1: "Ruby on Rails, or Rails, is a server-side web application framework written in Ruby under the MIT License.",
        //         description2: "Rails is a model-view-controller (MVC) framework that provides default structures for a database, a web service, and web pages.",
        //         status: "Active"
        //     },
        //     {
        //         title: "Flutter",
        //         tagLine: "UI toolkit for building natively compiled applications",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         description1: "Flutter is Google's UI toolkit for building natively compiled applications for mobile, web, and desktop from a single codebase.",
        //         description2: "Flutter allows developers to use a single codebase to deploy applications on multiple platforms, resulting in faster development and reduced maintenance costs.",
        //         status: "Active"
        //     },
        //     {
        //         title: "Swift 0.2",
        //         tagLine: "Powerful and intuitive programming language for iOS, macOS, watchOS, and tvOS",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         description1: "Swift is a general-purpose, multi-paradigm, compiled programming language developed by Apple Inc. for iOS, macOS, watchOS, and tvOS.",
        //         description2: "Swift is designed to be fast, safe, and expressive, with a syntax that is concise yet expressive.",
        //         status: "Active"
        //     },
        //     {
        //         title: "Spring Boot 0.2",
        //         tagLine: "Java-based framework for building web applications and microservices",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         description1: "Spring Boot is a Java-based framework used to create stand-alone, production-grade Spring-based applications quickly and easily.",
        //         description2: "Spring Boot provides a set of conventions for building production-ready applications with minimal configuration, allowing developers to focus on writing business logic.",
        //         status: "Active"
        //     },
        //     {
        //         title: "Vue.js 0.2",
        //         tagLine: "Progressive JavaScript framework",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         description1: "Vue.js is an open-source JavaScript framework used for building user interfaces and single-page applications.",
        //         description2: "Vue.js is designed to be incrementally adoptable, making it suitable for integrating into existing projects.",
        //         status: "Active"
        //     },
        //     {
        //         title: "Angular 0.2",
        //         tagLine: "Platform for building mobile and desktop web applications",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         description1: "Angular is a platform and framework for building single-page client applications using HTML and TypeScript.",
        //         description2: "Angular provides a comprehensive solution that includes tools for building applications with declarative templates, dependency injection, and end-to-end tooling.",
        //         status: "Active"
        //     },
        //     {
        //         title: "Node.js 0.2",
        //         tagLine: "JavaScript runtime built on Chrome's V8 JavaScript engine",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         description1: "Node.js is an open-source, cross-platform JavaScript runtime environment that executes JavaScript code outside of a browser.",
        //         description2: "Node.js is commonly used for building server-side and networking applications, and it allows developers to use JavaScript for both client-side and server-side scripting.",
        //         status: "Active"
        //     },
        //     {
        //         title: "Express.js 0.2",
        //         tagLine: "Fast, unopinionated, minimalist web framework for Node.js",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         description1: "Express.js is a web application framework for Node.js, designed for building web applications and APIs.",
        //         description2: "Express.js provides a thin layer of fundamental web application features, allowing developers to build robust web applications quickly and easily.",
        //         status: "Active"
        //     },  {
        //         title: "Python 9.9",
        //         tagLine: "High-level programming language",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png", 
        //         description1: "Python is a high-level, interpreted programming language known for its simplicity and readability.",
        //         description2: "Python is versatile and can be used for various applications such as web development, data science, artificial intelligence, and more.",
        //         status: "Active"
        //     },
        //     {
        //         title: "Java 0.5",
        //         tagLine: "Object-oriented programming language",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         description1: "Java is a class-based, object-oriented programming language designed for building portable applications.",
        //         description2: "Java is widely used in enterprise and web applications, Android app development, and more.",
        //         status: "Active"
        //     },
        //     {
        //         title: "C++",
        //         tagLine: "General-purpose programming language",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         description1: "C++ is a general-purpose programming language developed as an extension of the C programming language.",
        //         description2: "C++ is commonly used in systems programming, game development, and performance-critical applications.",
        //         status: "Active"
        //     },
        //     {
        //         title: "Go 0.8",
        //         tagLine: "Open-source programming language",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         description1: "Go, also known as Golang, is an open-source programming language developed by Google.",
        //         description2: "Go is known for its simplicity, efficiency, and concurrency support, making it suitable for building scalable and efficient software.",
        //         status: "Active"
        //     },
        //     {
        //         title: "Rust",
        //         tagLine: "Systems programming language",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         description1: "Rust is a systems programming language designed for safety, concurrency, and performance.",
        //         description2: "Rust aims to provide memory safety without garbage collection, making it suitable for systems programming tasks like operating systems and embedded devices.",
        //         status: "Active"
        //     },
        //     {
        //         title: "PHP 0.5",
        //         tagLine: "Server-side scripting language",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         description1: "PHP is a popular general-purpose scripting language that is especially suited to web development.",
        //         description2: "PHP is commonly used for creating dynamic web pages and web applications, and it can be embedded into HTML.",
        //         status: "Active"
        //     },
        //     {
        //         title: "WordPress 0.0.0",
        //         tagLine: "Open-source content management system",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         description1: "WordPress is an open-source content management system (CMS) based on PHP and MySQL.",
        //         description2: "WordPress is known for its ease of use and flexibility, making it a popular choice for building websites and blogs.",
        //         status: "Active"
        //     },
        //     {
        //         title: "HTML5",
        //         tagLine: "Markup language for structuring web pages",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         description1: "HTML5 is the latest version of the Hypertext Markup Language used for structuring and presenting content on the World Wide Web.",
        //         description2: "HTML5 introduces new features such as multimedia support, canvas drawing, and improved semantics.",
        //         status: "Active"
        //     },
        //     {
        //         title: "CSS3",
        //         tagLine: "Style sheet language for designing web pages",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         description1: "CSS3 is the latest version of Cascading Style Sheets used for styling the presentation of HTML and XML documents.",
        //         description2: "CSS3 introduces new features such as animations, transitions, and advanced layout techniques.",
        //         status: "Active"
        //     },
        //     {
        //         title: "JavaScript 0.2",
        //         tagLine: "High-level, interpreted programming language",
        //         icon: "coverimagetest.png",
        //         coverImg: "coverimagetest.png",
        //         description1: "JavaScript is a versatile programming language used for building interactive websites and web applications.",
        //         description2: "JavaScript is commonly used for client-side scripting, but it can also be used for server-side scripting with Node.js.",
        //         status: "Active"
        //     },
        //     // Add more entries to reach a total of 40
        // ];

        // for(const data of  categoriesData){
        //     const categoryData = new Category({
        //                 title: data.title,
        //                 tagLine: data.tagLine,
        //                 icon:data.icon   ,
        //                 coverImg:  data.coverImg,
        //                 popular_category: data.popular_category,
        //                 topcategory: data.topcategory,
        //                 description1: data.description1,
        //                 description2: data.description2,
        //             });
        // Add more entries to reach a total of 40 or more
        //     const softwareSkillsData = [
        //         {
        //             title: "JavaScript",
        //             tagLine: "High-level, interpreted programming language",
        //             popular_skill: true,
        //             status: "Active"
        //         },
        //         {
        //             title: "Python",
        //             tagLine: "High-level programming language",
        //             popular_skill: true,
        //             status: "Active"
        //         },
        //         {
        //             title: "Java",
        //             tagLine: "Object-oriented programming language",
        //             popular_skill: true,
        //             status: "Active"
        //         },
        //         {
        //             title: "C++",
        //             tagLine: "General-purpose programming language",
        //             popular_skill: true,
        //             status: "Active"
        //         },
        //         {
        //             title: "HTML5",
        //             tagLine: "Markup language for structuring web pages",

        //             status: "Active"
        //         },
        //         {
        //             title: "CSS3",
        //             tagLine: "Style sheet language for designing web pages",

        //             status: "Active"
        //         },
        //         {
        //             title: "React",
        //             tagLine: "JavaScript library for building user interfaces",

        //             status: "Active"
        //         },
        //         {
        //             title: "Node.js",
        //             tagLine: "JavaScript runtime environment",

        //             status: "Active"
        //         },
        //         {
        //             title: "Angular",
        //             tagLine: "Platform for building mobile and desktop web applications",

        //             status: "Active"
        //         },
        //         {
        //             title: "MongoDB",
        //             tagLine: "Document-oriented NoSQL database",

        //             status: "Active"
        //         },
        //         {
        //             title: "SQL",
        //             tagLine: "Structured Query Language for managing relational databases",

        //             status: "Active"
        //         },
        //         {
        //             title: "Git",
        //             tagLine: "Distributed version control system",

        //             status: "Active"
        //         },
        //         {
        //             title: "Docker",
        //             tagLine: "Platform for building, shipping, and running applications in containers",

        //             status: "Active"
        //         },
        //         {
        //             title: "Kubernetes",
        //             tagLine: "Open-source system for automating deployment, scaling, and management of containerized applications",

        //             status: "Active"
        //         },
        //         {
        //             title: "AWS",
        //             tagLine: "Amazon Web Services",

        //             status: "Active"
        //         },
        //         {
        //             title: "Azure",
        //             tagLine: "Cloud computing service by Microsoft",

        //             status: "Active"
        //         },
        //         {
        //             title: "Firebase",
        //             tagLine: "Platform for building web and mobile applications",

        //             status: "Active"
        //         },
        //         {
        //             title: "Bootstrap",
        //             tagLine: "Front-end framework for developing responsive and mobile-first websites",

        //             status: "Active"
        //         },
        //         {
        //             title: "Sass",
        //             tagLine: "CSS preprocessor",

        //             status: "Active"
        //         },
        //         {
        //             title: "Redux",
        //             tagLine: "State management library for JavaScript applications",

        //             status: "Active"
        //         },
        //         {
        //             title: "Express.js",
        //             tagLine: "Web application framework for Node.js",

        //             status: "Active"
        //         },
        //         {
        //             title: "Vue.js",
        //             tagLine: "Progressive JavaScript framework for building user interfaces",

        //             status: "Active"
        //         },
        //         {
        //             title: "TypeScript",
        //             tagLine: "Typed superset of JavaScript",

        //             status: "Active"
        //         },
        //         {
        //             title: "RESTful API",
        //             tagLine: "Application programming interface that uses HTTP requests to perform CRUD operations",

        //             status: "Active"
        //         },
        //         {
        //             title: "GraphQL",
        //             tagLine: "Query language for APIs",

        //             status: "Active"
        //         },
        //         {
        //             title: "Jira",
        //             tagLine: "Project management tool",

        //             status: "Active"
        //         },
        //         {
        //             title: "Confluence",
        //             tagLine: "Team collaboration software",

        //             status: "Active"
        //         },
        //         {
        //             title: "Visual Studio Code",
        //             tagLine: "Source-code editor",

        //             status: "Active"
        //         },
        //         {
        //             title: "Linux",
        //             tagLine: "Open-source operating system",

        //             status: "Active"
        //         },
        //         {
        //             title: "Windows",
        //             tagLine: "Operating system by Microsoft",

        //             status: "Active"
        //         }
        //     ];

        //     // Add more entries to reach a total of 30 or more


        // for(const data of  softwareSkillsData){
        //     let Skillsdata = await Skills.create(data)

        // }


        const subcategoriesData = [
            {
                category_id: "66278b8db95d0175d303e20b",
                title: "Data Modeling",
                tagLine: "Designing MongoDB Schemas",
            },
            {
                category_id: "66278b8db95d0175d303e20b",
                title: "Query Optimization",
                tagLine: "Improving MongoDB Query Performance",
            },
            {
                category_id: "66278b8db95d0175d303e20b",
                title: "Replication",
                tagLine: "Setting Up MongoDB Replication",
            },
            {
                category_id: "66278c771697cee93aa85d78",
                title: "Web Development",
                tagLine: "Creating Web Applications with Python",
            },
            {
                category_id: "66278c771697cee93aa85d78",
                title: "Data Analysis",
                tagLine: "Analyzing Data Using Python Libraries",
            },
            {
                category_id: "66278c771697cee93aa85d78",
                title: "Machine Learning",
                tagLine: "Implementing Machine Learning Algorithms in Python",
            },
            {
                category_id: "66278b8db95d0175d303e20d",
                title: "Android App Development",
                tagLine: "Building Mobile Apps with Java",
            },
            {
                category_id: "66278b8db95d0175d303e20d",
                title: "Enterprise Applications",
                tagLine: "Developing Enterprise Software with Java",
            },
            {
                category_id: "66278b8db95d0175d303e20d",
                title: "Game Development",
                tagLine: "Creating Games Using Java Technologies",
            }, {
                category_id: "66278b8db95d0175d303e213",
                title: "RESTful APIs",
                tagLine: "Building RESTful APIs with Node.js",
            },
            {
                category_id: "66278b8db95d0175d303e213",
                title: "Real-time Applications",
                tagLine: "Developing Real-time Applications using Node.js",
            },
            {
                category_id: "66278b8db95d0175d303e213",
                title: "Authentication",
                tagLine: "Implementing Authentication in Node.js Applications",
            },
            {
                category_id: "66278b8db95d0175d303e20f",
                title: "Component Libraries",
                tagLine: "Using Component Libraries in React Applications",
            },
            {
                category_id: "66278b8db95d0175d303e20f",
                title: "State Management",
                tagLine: "Managing State in React Applications",
            },
            {
                category_id: "66278b8db95d0175d303e20f",
                title: "Server-Side Rendering",
                tagLine: "Implementing Server-Side Rendering with React",
            },
            {
                category_id: "66278b8db95d0175d303e211",
                title: "Dependency Injection",
                tagLine: "Understanding Dependency Injection in Angular",
            },
            {
                category_id: "66278b8db95d0175d303e211",
                title: "Forms and Validation",
                tagLine: "Handling Forms and Validation in Angular Applications",
            },
            {
                category_id: "66278b8db95d0175d303e211",
                title: "Routing",
                tagLine: "Implementing Routing in Angular Applications",
            },
            // Add more subcategories for the remaining categories
        ];

        // Add more entries for the remaining categories


        for (const data of subcategoriesData) {
            const categoryData = new SubCategory({
                title: data.title,
                tagLine: data.tagLine,
                category_id: data.category_id

            });
            await categoryData.save()
        }

        res.status(200).json({
            message: "Add Successfully ",
            status: "1",
        })



    } catch (error) {
        console.log(error);
        return res.status(400).json({
            message: " error", error,
            status: "0",
        });

    }
};

// List of possible characters for generating random strings
const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

// Function to generate a random string of given length
function generateRandomString(length) {
    let result = '';
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return result;
}

// Function to generate a random phone number
function generatePhoneNumber() {
    let phoneNumber = '';
    for (let i = 0; i < 7; i++) {
        phoneNumber += Math.floor(Math.random() * 10);
    }
    return phoneNumber;
}

// Function to generate a random email address
function generateEmail() {
    const username = generateRandomString(8);
    const domain = ['gmail.com', 'yahoo.com', 'hotmail.com', 'outlook.com'][Math.floor(Math.random() * 4)];
    return `${username}@${domain}`;
}

// Function to generate a random WhatsApp number
function generateWhatsAppNumber() {
    let whatsappNumber = '';
    for (let i = 0; i < 7; i++) {
        whatsappNumber += Math.floor(Math.random() * 10);
    }
    return whatsappNumber;
}

// List of professional types
const professionalTypes = ["Engineer", "Doctor", "Teacher", "Designer", "Developer", "Manager"];

// List of countries, states, and cities
const countries = ["Pakistan", "India", "USA", "UK", "Canada", "Australia"];


// Test accessing country and country code
console.log(countries[0].country); // Output: Pakistan
console.log(countries[0].country_code); // Output: PK

const states = ["Punjab", "Sindh", "Balochistan", "Khyber Pakhtunkhwa", "Gilgit-Baltistan", "Azad Kashmir"];
const cities = ["Lahore", "Karachi", "Islamabad", "Rawalpindi", "Faisalabad", "Multan"];


const postmanaddprovider = async (req, res) => {

    try {


        for (let i = 0; i < 200; i++) {

            let UserData = new User({
                email: generateEmail(),
                login_method: "Custom",
                password: "12345678",
                phonenum: generatePhoneNumber(),
                whatsappnum: generateWhatsAppNumber(),
                firstname: generateRandomString(5),

                role: "662787d7b19ed292b223b6d4"

            })



            await UserData.save()
                .then(async result => {
                    console.log("result", result);
                    let userid = result._id
                    const ServiceProviderData = new ServiceProvider({
                        name: generateRandomString(5),
                        professional_type: professionalTypes[Math.floor(Math.random() * professionalTypes.length)],
                        experience: "2-3 years",
                        website_url: "www.aiksol.org",
                        country: countries[Math.floor(Math.random() * countries.length)],
                        state: states[Math.floor(Math.random() * states.length)],
                        city: cities[Math.floor(Math.random() * cities.length)],
                        worldwide: true,
                        company_name: "Aiksol",
                        region: false

                    });
                    await ServiceProviderData.save().then(async result => {
                        console.log("result", result);



                        const TrustPointsData = new TrustPoints({
                            email_verified: true,
                            phone_verified: true,
                            //    whatsapp_verified: true,
                            user_id: userid,
                            totalpoints: 20,

                        });

                        await db.TrustPoints.create(TrustPointsData)
                            .then(result => {
                                console.log("result", result);
                            })
                            .catch(error => {
                                console.log("error", error);

                            });



                    })
                        .catch(error => {
                            console.log("error", error);

                        });

                }).catch(error => {
                    console.log("error", error);
                    return res.status(200).json({
                        message: "Registered failed",
                        status: "0",
                    });
                });


        } return res.status(200).json({
            message: "Registered Successfully",
            status: "1",
        });
    } catch (error) {
        console.log(error);
        return res.status(400).json({
            message: " error", error,
            status: "0",
        });

    }
}

const languages = ['English', 'Spanish', 'French', 'German', 'Chinese', 'Arabic', 'Hindi', 'Russian', 'Portuguese', 'Bengali', 'Urdu', 'Japanese', 'Punjabi', 'Telugu', 'Marathi', 'Tamil', 'Turkish', 'Italian', 'Thai', 'Korean'];

// Function to generate a random description
function generateRandomDescription() {
    const templates = [
        "Providing top-notch services tailored to meet your needs.",
        "Bringing years of expertise to deliver exceptional results.",
        "Dedicated to delivering excellence in every service we offer.",
        "Your trusted partner for all your service needs.",
        "Creating unforgettable experiences with our professional services.",
        "Your satisfaction is our priority, and we go above and beyond to achieve it.",
        "Combining creativity and skill to deliver outstanding service.",
        "Elevating your experience with our innovative solutions.",
        "Transforming ideas into reality with our expert services.",
        "Our commitment to quality ensures you get the best service every time."
    ];

    return templates[Math.floor(Math.random() * templates.length)];
}

// Function to generate random entries
function generateRandomEntries(numEntries) {
    const entries = [];
    for (let i = 0; i < numEntries; i++) {
        const entry = {
            title: `Service Provider ${i + 1}`,
            language: [languages[Math.floor(Math.random() * languages.length)]],
            description: generateRandomDescription()
        };
        entries.push(entry);
    }
    return entries;
}

async function getRandomEntriesWithRandomServices(numEntries) {
    try {
      // Get total count of entries in the database
      const totalCount = await Category.countDocuments();
  
      // Generate random indices to select random entries
      const randomIndices = [];
      while (randomIndices.length < numEntries) {
        const randomIndex = Math.floor(Math.random() * totalCount);
        if (!randomIndices.includes(randomIndex)) {
          randomIndices.push(randomIndex);
        }
      }
  
      // Fetch random entries based on random indices
      const randomEntries = await Category.find().skip(randomIndices).limit(numEntries);
  
      // Iterate through each random entry and select a random service
      const entriesWithRandomServices = randomEntries.map(entry => {
        const randomServiceIndex = Math.floor(Math.random() * entry.services.length);
        const randomService = entry.services[randomServiceIndex];
        return {
          entry,
          randomService
        };
      });
  
      return entriesWithRandomServices;
    } catch (error) {
      console.error("Error fetching random entries:", error);
      throw error;
    }
  }



const postmancategoriesofprovider = async (req, res) => {

    try {
      
        let entry=generateRandomEntries(1)

        const updateuserprofile = await User.updateOne(
            {
                _id: req.body.user_id
            },
            {
                $set: {
                    language: entry[0]?.language,
                    title: entry[0]?.title,

                    description: entry[0]?.description
                }
            }
        )

        
        const numEntriesToFetch = 1; // You can specify the number of random entries you want to fetch
        let categories=await getRandomEntriesWithRandomServices(numEntriesToFetch)
           

        const updateuser = await ServiceProvider.updateOne(
            {
                user_id: req.body.user_id,
            },
            {
                $push: {
                    categories: categories[0].entry.id,
                    skills: req.body.skills,
                },
            }
        )
            .then((result) => {


                return res.status(200).json({
                    message: "Updated Successfully",
                    status: "1",
                });
            })
            .catch((error) => {
                console.log("error", error);
                return res.status(400).json({
                    message: "Updating failed",
                    status: "0",
                });
            });








    } catch (error) {
        console.log(error);
        return res.status(400).json({
            message: "Error :/", error,
            status: "0",
        });

    }
};

const getproviderids = async (req, res) => {

    try {



        let updateuser = await ServiceProvider.find().select('user_id _id skills')

        let newArr = []
        updateuser.forEach((item,index)=>{
            if (!item.skills.length) {
               newArr.push(item)
            }
        })

        
        
        return res.status(200).json({
            newArr

        });



    } catch (error) {
        console.log(error);
        return res.status(400).json({
            message: "Error :/", error,
            status: "0",
        });

    }
};
module.exports = { postmanaddprovider, postmanaddCategory, getproviderids };
