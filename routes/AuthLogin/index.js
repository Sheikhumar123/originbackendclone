const routers = require("express").Router();

var fs = require("fs");
const multer = require("multer");
const path = require("path");
const {loginuser,verifypin,forgetpassword,changepassword,updatenewpassword,verifyemail,verifynumber} = require("./loginuser");


const {registerinvestor,referaluserapi,getcodeamount,investortransactionsadd} = require("../Investors/registerinvestor");
const {getQRdetail,} = require("../AdminRoutes/QRdetails");


routers.post("/changepassword", changepassword);
routers.post("/forgetpassword", forgetpassword);
routers.post("/loginuser", loginuser);
routers.post("/verifynumber", verifynumber);
routers.post("/updatenewpassword", updatenewpassword);
routers.post("/verifypin", verifypin);
routers.post("/verifyemail", verifyemail);


//investors api 
routers.post("/registerinvestor", registerinvestor);
routers.post("/referaluserapi", referaluserapi);
routers.get("/getcodeamount", getcodeamount);
routers.get("/getQRdetail", getQRdetail);
const storagetransactionss = multer.diskStorage({
    destination: (req, file, cb) => {
      if (file.fieldname === "transaction_ss") {
        const destinationPath = path.join(
          process.cwd(),
          "/uploads/transactions/"
        );
  
        // Create the destination directory if it doesn't exist
        if (!fs.existsSync(destinationPath)) {
          fs.mkdirSync(destinationPath, { recursive: true });
        }
  
        cb(null, destinationPath);
      } else {
        cb(new Error("Invalid fieldname"));
      }
    },
    filename: (req, file, cb) => {
      if (file.fieldname === "transaction_ss") {
        cb(null, file.fieldname + "-" + Date.now() + ".jpg");
      } else {
        cb(new Error("Invalid fieldname"));
      }
    },
  });
  var uploadtransactionss = multer({ storage: storagetransactionss });
  routers.post("/investortransactionsadd", uploadtransactionss.fields([
    {
      name: "transaction_ss",
      maxCount: 1,
    },
  ]), investortransactionsadd);
module.exports = routers;
