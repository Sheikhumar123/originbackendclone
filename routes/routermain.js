const routes = require("express").Router();
const routers = require("express").Router();

const auth = require("./AuthLogin");
const landingpage = require("./LandingpageApis");
const dashboard = require("./Dashboards");
const investor = require("./Investors");
const admindata = require("./AdminRoutes/index.js");
const userdata = require("./ServiceProviders/index.js");
const {postmanaddprovider,postmanaddCategory,getproviderids} = require("./fortesting apis.js");

const { authMiddleWare} = require("../middleware/mainAuth");


routes.use("/landingpage", landingpage);
routes.use("/auth", auth);
routes.use("/userdata", userdata);
routes.use("/dashboard",authMiddleWare,dashboard);
routes.use("/admindata",authMiddleWare, admindata);
routes.use("/investor",authMiddleWare,investor);
// routes.use("/", (req,res)=>{
//     res.json("Origin Api Running")
// });




//// for testingggg 
routes.post("/postmanaddprovider", postmanaddprovider);
routes.post("/postmanaddCategory", postmanaddCategory);
routes.get("/getproviderids", getproviderids);
module.exports = routes;
