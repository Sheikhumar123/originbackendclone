const db = require("../../db");
let Investortransaction = db.Investortransaction
let Investor = db.Investor
let Withdrawl = db.Withdrawl
const approveinvestortransaction = async (req, res) => {

    try {
        const transactiondata = await Investortransaction.findOne({
            _id: req.body.transaction_id
        });
        if (!transactiondata) {
            return res.status(400).json({
                message: "No Transaction found",
                status: "0",
            });
        }
        if (!transactiondata?.status=="Pending") {
            return res.status(400).json({
                message: "The transaction status is already updated and cannot be modified.",
                status: "0",
            });
        }

        const updateuser = await Investortransaction.updateOne(
            {
                _id: req.body.transaction_id
            },
            {
                $set: {

                    status:req.body?.status,
                    confirmation: req.body?.confirmation,
          
                },
            }
        )
            .then(async (result) => {
                if (req.body?.status == "Received"){

                // const updateuser = await Investor.updateOne(
                //     {
                //         _id: req.body.id
                //     },
                //     {
                //         $inc: {
        
                            
                //             credit_amount: +req.body?.credit_amount,
                //             investment_amount: +req.body?.investment_amount,
                  
                //         },
                //     }
                // ) 
                const updateFields = {};
                const findinvestor = await Investor.findOne({ user_id: transactiondata?.user_id }).select('purchase_amount credit_amount investment_amount')
if (transactiondata?.type =="Investment") {
    
    updateFields.investment_amount =Number(findinvestor?.investment_amount) +Number(transactiondata?.amount);
}

else if (transactiondata?.type =="Credit") {


    let  credit =  Number(transactiondata?.amount) *0.1
    let purchaseamount=findinvestor?.purchase_amount? Number(transactiondata?.amount) + Number(findinvestor?.purchase_amount):Number(transactiondata?.amount)
    let totalcredit=findinvestor?.credit_amount? Number(transactiondata?.amount)  + Number(findinvestor?.credit_amount):Number(transactiondata?.amount)
                        updateFields.purchase_amount = purchaseamount
                        updateFields.credit_amount = Number(totalcredit) + credit
   // updateFields.credit_amount = Number(findinvestor?.credit_amount) +Number(transactiondata?.amount);
}

const updateuser = await Investor.updateOne(
    { user_id: transactiondata?.user_id },
    { $set: updateFields }
);   
                }
                

                return res.status(200).json({
                    message: "Updated Successfully",
                    status: "1",
                });
            })
            .catch((error) => {
                console.log("error", error);
                return res.status(400).json({
                    message: "Updating failed",
                    status: "0",
                });
            });


     
   



    } catch (error) {
        console.log(error);
        return res.status(400).json({
            message: " error", error,
            status: "0",
        });

    }
};
const approveinvestorwithdraw = async (req, res) => {

    try {
        const transactiondata = await Withdrawl.findOne({
            _id: req.body.transaction_id
        });
        if (!transactiondata) {
            return res.status(400).json({
                message: "No Transaction found",
                status: "0",
            });
        }


        const updateuser = await Withdrawl.updateOne(
            {
                _id: req.body.transaction_id
            },
            {
                $set: {

                    status:req.body?.status,
                    confirmation: req.body?.confirmation,
          
                },
            }
        )
            .then(async (result) => {
               
                    let updateFields={}
                    const findinvestor = await Investor.findOne({ user_id: transactiondata.user_id, }).select('referal_amount investment_amount withdrawl_amount')
              if (req.body?.status == "Received"){  
                updateFields.withdrawl_amount =Number(findinvestor?.withdrawl_amount) - Number(transactiondata.amount)
               } if (req.body?.status == "Decline"){  
                updateFields.withdrawl_pending =Number(findinvestor?.withdrawl_pending) - Number(transactiondata.amount)
               } 
                    // if (transactiondata?.type == "Investment Income") {

                    //     updateFields.investment_amount = Number(findinvestor?.investment_amount) - Number(transactiondata.amount)
                    // }
    
                    // else if (transactiondata?.type == "Referral Income") {
                    //     updateFields.referal_amount = Number(findinvestor?.referal_amount) -Number(transactiondata.amount);
                    // }
                    
                      
                    
    
                 
    
                    const updateuser = await Investor.updateOne(
                        { user_id: transactiondata.user_id, },
                        { $set: updateFields }
                    ).then(async (result) => {
                      
    
    
                    })
                        .catch(error => {
                            console.log("error", error);
                            return res.status(200).json({
                                message: "Transaction failed",
                                status: "0",
                            });
                        })   
                

                return res.status(200).json({
                    message: "Updated Successfully",
                    status: "1",
                });
            })
            .catch((error) => {
                console.log("error", error);
                return res.status(400).json({
                    message: "Updating failed",
                    status: "0",
                });
            });


     
   



    } catch (error) {
        console.log(error);
        return res.status(400).json({
            message: " error", error,
            status: "0",
        });

    }
};
const getinvestortransaction = async (req, res) => {

    try {
let Confirmation=req.body.confirmation
if(Confirmation ){
    var transactions = await Investortransaction.find({confirmation:Confirmation});
}else{

    var transactions = await Investortransaction.find();
}

        return res.status(200).json({
            Transactions:transactions,
            status: "1",
        });



    } catch (error) {
        console.log(error);
        return res.status(400).json({
            message: "updating error", error,
            status: "0",
        });

    }
};

module.exports = { getinvestortransaction, approveinvestorwithdraw,approveinvestortransaction,  };
