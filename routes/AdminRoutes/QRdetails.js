const db = require("../../db");
let QRdetails = db.QRdetails
const config = require("./../../config.json");
const addQRdetails = async (req, res) => {

    try {
        // var categoryfind = await db.QRdetails.findOne({ bussiness_id: req.body.bussiness_id, });
        // if (categoryfind) {
        //     return res.status(200).json({ message: "QRdetails with same BussinessId already exist", status: "0", });
        // }
        if (typeof req?.files?.qr_code != "undefined") {
            var images = "";
      
            for (var j = 0; j < req.files.qr_code.length; j++) {
              var image_name = req.files.qr_code[j].filename;
              images += image_name + ",";
              var imagedata = images.replace(/,\s*$/, "");
            }
          } else {
            var imagedata = req.body.qr_code;
          }
        var categoryfind = await db.QRdetails.findOne({ status:'Active' });
        if (categoryfind) {
            const category = await QRdetails.updateOne({
                _id: categoryfind?.id
            }, {
                $set: {
                    bussiness_name: req.body.bussiness_name,
                    bussiness_id: req.body.bussiness_id,
                  qr_code:  imagedata,
                },
    
            }) .then(async (result) => {
                console.log("result", result);
          
                    return res.status(200).json({
                        message: "added Successfully",
                        status: "1",
                    });
    
    
    
    
            })
            .catch(error => {
                console.log("error", error);
                return res.status(200).json({
                    message: "adding failed",
                    status: "0",
                });
            });
        }else{
 let categoryData = new QRdetails({
            bussiness_name: req.body.bussiness_name,
            bussiness_id: req.body.bussiness_id,
          qr_code:  imagedata,
           
        }) ;     
         await categoryData.save()
        .then(async (result) => {
            console.log("result", result);
      
                return res.status(200).json({
                    message: "added Successfully",
                    status: "1",
                });




        })
        .catch(error => {
            console.log("error", error);
            return res.status(200).json({
                message: "adding failed",
                status: "0",
            });
        });
        }
      
         
       

     



    } catch (error) {
        console.log(error);
        return res.status(400).json({
            message: " error", error,
            status: "0",
        });

    }
};
const getQRdetail = async (req, res) => {
    try {
        const qrdetailsget = await QRdetails.findOne({status:'Active'})
        if (__dirname == "C:\Client\OORIGIN\OoriginBackend\routes\QRdetails") {
            var qrPhotosurl =
                process.env.BackendApi + "/api/uploads/qrdetails/";
        } else {

            var qrPhotosurl =
                config?.IMAGE_URL + req.get("host") + "/api/uploads/qrdetails/";
        }
        let qrdetails ={
            ...qrdetailsget?._doc,
            qr_code:qrPhotosurl+qrdetailsget?.qr_code
        }
        return res.status(200).json({
            qrdetails,
            message: "QRdetails fetched Successfully",
            status: "1",
        });
    } catch (error) {
        console.log(error);
        return res.status(400).json({
            message: error,
            status: "0",
        });
    }
};


// const categoryDelete = async (req, res) => {
//     try {
//         const category = await QRdetails.findByIdAndDelete(req.body.id)

//         if (category) {
//             res.status(200).json({
//                 message: "QRdetails deleted Successfully",
//                 status: "1",
//             });
//         } else {
//             res.status(200).json({
//                 message: "something went wrong",
//                 status: "0",
//             })
//         }


//     } catch (error) {
//         console.log(error);
//         return res.status(400).json({
//             message: error,
//             status: "0",
//         });
//     }
// };


module.exports = { addQRdetails,getQRdetail };
