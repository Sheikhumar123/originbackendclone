const db = require("../../db");
let User = db.User
let Roles = db.Roles
let ReferncesUsers = db.ReferncesUsers
let Investortransaction = db.Investortransaction
let CreditCodes = db.CreditCodes
let Investor = db.Investor
const config = require("./../../config.json");

// generate Referal code
const Referalcode = async () => {

    try {
        let otp;
        return (otp = `${Math.floor(1000 + Math.random() * 90000000)}`);


    } catch (error) {
        throw error;

    }
};
const registerinvestor = async (req, res) => {

    try {
        var InvestorRoleID = await db.Roles.findOne({ role_name: "Investor" });
        let ruleid = InvestorRoleID?.id
        if (!InvestorRoleID) {
            const InfluncerData = new Roles({
                role_name: "Investor"


            });
            await InfluncerData.save().then(async result => {
                console.log("result", result);
                ruleid = result?.id
            })
                .catch(error => {
                    console.log("error", error);

                });
        }

        const userdata = await User.findOne({
            email: req.body.email
        });
        if (userdata) {
            return res.status(400).json({ message: "User already exist", status: "0", });
        }

        if (req.body.reference_code) {
            let investorrdata = await Investor.findOne({
                referal_code: req.body.reference_code
            })
            if (!investorrdata) {
                return res.status(400).json({ message: "Invalid Reference Code", status: "0", });
            }
            let totamount = Number(req.body.amount) * 0.1
            let referalamount = Number(investorrdata?.referal_amount) + totamount
            const updateuser = await Investor.updateOne(
                {
                    _id: investorrdata._id,
                },
                {
                    $set: {

                        referal_amount: referalamount,


                    },

                }, {
                $push: {

                    //  referedusers:


                },

            }
            )

        }
        if (req.body.login_method == "Google") {
            UserData = new User({
                email: req.body.email,
                login_method: 'Google',
                social_name: req.body.social_name,
                phonenum: req.body.phonenum,
                dob: req.body.dob,
                firstname: req.body.firstname,
                lastname: req.body.lastname,

                fathername: req.body.fathername,
                address: req.body.address,

                country_code: req.body.country_code,
                social_id: req.body.social_id,
                role: ruleid,

            });
        } else {
            UserData = new User({
                email: req.body.email,
                login_method: 'Custom',
                password: req.body.password,
                phonenum: req.body.phonenum,
                dob: req.body.dob,
                firstname: req.body.firstname,
                lastname: req.body.lastname,

                fathername: req.body.fathername,
                address: req.body.address,

                role: ruleid,
                country_code: req.body.country_code,
                username: req.body.name,

            })
        }

        await UserData.save()
            .then(async (result) => {
                console.log("result", result);


                let InvestorData = new Investor({
                    referal_code: await Referalcode(),
                    user_id: result.id,
                    // isreference:req.body.reference_code

                })


                await InvestorData.save().then(async (result) => {
                    console.log("result", result);
                })
                    .catch(error => {
                        console.log("error", error);
                        return res.status(200).json({
                            message: "Registered failed",
                            status: "0",
                        });
                    })
                return res.status(200).json({
                    message: "Registered Successfully", user_id: result.id,
                    status: "1",
                });
            })
            .catch(error => {
                console.log("error", error);
                return res.status(200).json({
                    message: "Registered failed",
                    status: "0",
                });
            });
    }






    catch (error) {
        console.log(error);
        return res.status(400).json({
            message: "Error :/", error,
            status: "0",
        });

    }
};
const referaluserapi = async (req, res) => {

    try {
        if (!req.body.reference_code) {
            return res.status(200).json({ message: "Successfully", status: "1", });
        }

        const userdata = await User.findOne({
            _id: req.body.user_id
        });
        if (!userdata) {
            return res.status(400).json({ message: "User not found", status: "0", });
        }

        if (req.body.reference_code) {
            let investorrdata = await Investor.findOne({
                referal_code: req.body.reference_code
            })
            if (!investorrdata) {
                return res.status(400).json({ message: "Invalid Reference Code", status: "0", });
            }
            let totamount = Number(req.body.amount) * 0.1
            let referalamount = Number(investorrdata?.referal_amount) + totamount
            let withdrawlpending = Number(investorrdata?.withdrawl_amount) + totamount
            const updateuser = await Investor.updateOne(
                {
                    _id: investorrdata._id,
                },
                {
                    $set: {

                        referal_amount: referalamount,
                        withdrawl_amount: withdrawlpending,


                    },
                    $push: {

                        referedusers: { amount: totamount, user_id: req.body.user_id }


                    },
                },



            ).then(async (result) => {
                const updateuser = await Investor.updateOne(
                    {
                        user_id: req.body.user_id,
                    },
                    {
                        $set: {
                            isreference: req.body.reference_code,
                        },

                    },



                )
                console.log(result)
                const referncesdata = await ReferncesUsers.findOne({ totalusers: { $ne: null } });
                if (referncesdata && referncesdata?.totalusers >= 5000) {
                    return res.status(400).json({ message: "NO MORE REFERNCES CAN BE ADDED", status: "0", });
                }
                if (referncesdata) {

                    const updateuser = await ReferncesUsers.updateOne(
                        {
                            _id: referncesdata?.id,
                        },
                        {
                            $inc: {
                                totalusers: 1
                            },
                        }
                    )
                } else {
                    let InvestorrrData = new ReferncesUsers({
                        totalusers: 1
                    })


                    await InvestorrrData.save().then(async (result) => {
                        console.log("result", result);
                    })
                }



                return res.status(200).json({
                    message: "Updated Successfully",
                    status: "1",
                });
            })
                .catch((error) => {
                    console.log("error", error);
                    return res.status(400).json({
                        message: "Updating failed",
                        status: "0",
                    });
                });

        }







    } catch (error) {
        console.log(error);
        return res.status(400).json({
            message: "Error :/", error,
            status: "0",
        });

    }
};
const investortransactionsadd = async (req, res) => {

    try {

 
       console.log(req.body)

        const userdata = await User.findOne({
            _id: req.body.user_id
        });
        if (!userdata) {
            return res.status(400).json({ message: "User not exist", status: "0", });
        }
        if (typeof req?.files?.transaction_ss != "undefined") {
            var images = "";

            for (var j = 0; j < req.files.transaction_ss.length; j++) {
                var image_name = req.files.transaction_ss[j].filename;
                images += image_name + ",";
                var imagedata = images.replace(/,\s*$/, "");
            }
        } else {
            var imagedata = req.body.transaction_ss;
        }
       
        if(req.body.payment_method =="UPI" && !imagedata){
            return res.status(400).json({ message: "Transaction Screenshot is required", status: "0", });
           
        }
        if(req.body.payment_method =="Cash"){

            const findcredit = await CreditCodes.findOne(
                {
                    code: req.body.code,
                },)
                if (!findcredit) {
                    return res.status(400).json({ message: "No code found", status: "0", });
                }
                if (findcredit.status == "Used") {
                    return res.status(400).json({ message: "Code already used", status: "0", });
                }
            const updateuser = await CreditCodes.updateOne(
                {
                    code: req.body.code,
                },
                {
                    $set: {
                        usedBy_id:req.body.user_id,
                        status: "Used",
        
        
                    },
                }
            ).then(async (result) => {
                console.log("result", result);
        
        
            })
                .catch(error => {
                    console.log("result", result);
                })
                var amount = findcredit?.amount
        }else{
            var amount =req.body.amount
            //let fee = +req.body.fee /100
           // var total =Number(req.body.amount ) *fee
            var totalamount =Number(req.body.amount ) +Number(req.body.fee )
        }
        let UserData = new Investortransaction({
            user_id: req.body.user_id,
            code: req.body.code,
            amount: amount,
            total_amount: req.body.total_amount,
            fee: req.body.fee,
            transaction_ss: imagedata,
            payment_method: req.body.payment_method,
            type: req.body.type,


        });


        await UserData.save()
            .then(async (result) => {
                console.log("result", result);

                const updateuser = await Investor.updateOne(
                    {
                        user_id: req.body.user_id,
                    },
                    {
                        $push: {

                            transaction_ids: result?.id,


                        },
                    }
                ).then(async (result) => {
                    console.log("result", result);


                })
                    .catch(error => {
                        console.log("error", error);
                        return res.status(200).json({
                            message: "Transaction failed",
                            status: "0",
                        });
                    })
                return res.status(200).json({
                    message: "Transaction Request sent to Admin Successfully",
                    status: "1",
                });
            })
            .catch(error => {
                console.log("error", error);
                return res.status(200).json({
                    message: "Transaction failed",
                    status: "0",
                });
            });
    }






    catch (error) {
        console.log(error);
        return res.status(400).json({
            message: "Error :/", error,
            status: "0",
        });

    }
};
const getcodeamount = async (req, res) => {

    try {

        let userdata = await CreditCodes.findOne({
            code: req.query.code,
        })
        if (!userdata) {
            return res.status(400).json({ message: "code not exist", status: "0", });
        } 
        if (userdata.status == "Used") {
            return res.status(400).json({ message: "Code already used", status: "0", });
        }



        return res.status(200).json({
            message: "Code Verified Successfully",
            amount: userdata?.amount,
            status: "1",
        });






    } catch (error) {
        console.log(error);
        return res.status(400).json({
            message: "Error :/", error,
            status: "0",
        });

    }
};
const investorprofileupt = async (req, res) => {

    try {
        if (!req.body.user_id) {
            return res.status(400).json({
                message: "user_id required ",
                status: "0",
            });}
        console.log(__dirname)
   
        if (__dirname == "C:\Client\OORIGIN\OoriginBackend\routes\Investors") {
            var ProfilePhotosurl =
                process.env.BackendApi + "/api/uploads/profileimages/";
        } else {

            var ProfilePhotosurl =
                config?.IMAGE_URL + req.get("host") + "/api/uploads/profileimages/";
        }
        const userdata = await User.findOne({
            _id: req.body.user_id
        });
        if (!userdata) {
            return res.status(400).json({
                message: "No user found",
                status: "0",
            });
        }


        if (req.body?.email) {
            const phonenum = await User.findOne({
                email: req.body?.email, _id: { $ne: req.body.user_id }
            });
            if (phonenum) {
                return res.status(200).json({
                    message: "User with same email already exist",
                    status: "0",
                });
            }
        }


        if (typeof req?.files?.profile_image != "undefined") {
            var images = "";

            for (var j = 0; j < req.files.profile_image.length; j++) {
                var image_name = req.files.profile_image[j].filename;
                images += image_name + ",";
                var imagedata = images.replace(/,\s*$/, "");
            }
        } else {
            var imagedata = req.body.profile_image;
        }
        //  const newpassword = await bcrypt.hashSync(req.body.password, 10);

        const updateuser = await User.updateOne(
            {
                _id: req.body.user_id,
            },
            {
                $set: {

                    profile_image: imagedata,
                    lastname: req.body?.lastname,
                    firstname: req.body?.firstname,
                    email: req.body?.email,
                    address: req.body?.address,
                    state: req.body?.state,
                    city: req.body?.city,
                    //  password: newpassword,
                    //username: req.body?.username



                },
            }
        )
            .then(async (result) => {


                return res.status(200).json({
                    message: "Updated Successfully",profile_image_url: ProfilePhotosurl + imagedata,
                    status: "1",
                });
            })
            .catch((error) => {
                console.log("error", error);
                return res.status(400).json({
                    message: "Updating failed",
                    status: "0",
                });
            });








    } catch (error) {
        console.log(error);
        return res.status(400).json({
            message: "Error :/", error,
            status: "0",
        });

    }
};
const getsingleinvestorprofile = async (req, res) => {

    try {

        const userdata = await User.findOne({
            _id: req.query.user_id
        });
        if (!userdata) {
            return res.status(400).json({
                message: "No user found",
                status: "0",
            });
        }
        if (__dirname == "C:\Client\OORIGIN\OoriginBackend\routes\LandingpageApis") {

            var ProfilePhotosurl =
                process.env.BackendApi + "/api/uploads/profileimages/";
        } else {

            var ProfilePhotosurl =
                config?.IMAGE_URL + req.get("host") + "/api/uploads/profileimages/";
        }
        const updateuser = {

            profile_image: ProfilePhotosurl + userdata?.profile_image,
            ...userdata?._doc

        }


        return res.status(200).json({
            updateuser,
            status: "1",
        });






    } catch (error) {
        console.log(error);
        return res.status(400).json({
            message: "Error :/", error,
            status: "0",
        });

    }
};


const getusertraqnsactions = async (req, res) => {

    try {
        const limit = 10;
        if (req.body.page) {
            var page = req.body.page;
        } else {
            var page = 1;
        }
        const skipIndex = (page - 1) * limit;
        const userdata = await Investor.findOne({
            user_id: req.body.user_id
        });
        if (!userdata) {
            return res.status(400).json({
                message: "No user found",
                status: "0",
            });
        }
        console.log(__dirname)
        if (__dirname == "C:\Client\OORIGIN\OoriginBackend\routes\Investors") {

            var ProfilePhotosurl =
                process.env.BackendApi + "/api/uploads/transactions/";
        } else {

            var ProfilePhotosurl =
                config?.IMAGE_URL + req.get("host") + "/api/uploads/transactions/";
        }
        if (req.body.type) {
            var alldata = await Investortransaction.find({
                user_id: req.body.user_id, type: req.body.type
            }).limit(limit)
                .skip(skipIndex)
                .exec();
            var countdata = await Investortransaction.countDocuments({ user_id: req.body.user_id, type: req.body.type });
        } else {
            var alldata = await Investortransaction.find({
                user_id: req.body.user_id,
            }).limit(limit)
                .skip(skipIndex)
                .exec();
            var countdata = await Investortransaction.countDocuments({ user_id: req.body.user_id, });


        }

        let transactions = []



        for (const data of alldata) {
            transactions.push({
                ...data?._doc,
                transaction_ss: ProfilePhotosurl + data?.transaction_ss,
            })
        }




        return res.status(200).json({
            vendor_status: userdata?.vendor_status,
            // credit_available: userdata?.credit_amount,
            // codepurchased_amount: userdata?.codepurchased_amount,
            // code_used: userdata?.credit_amount,
            Transactions: transactions, limit: limit, count: countdata,
            status: "1",
        });






    } catch (error) {
        console.log(error);
        return res.status(400).json({
            message: "Error :/", error,
            status: "0",
        });

    }
};
const getinvestordashboard = async (req, res) => {

    try {

        let userdata = await Investor.findOne({
            user_id: req.body.user_id
        }).populate({
            path: 'user_id',
            select: 'username _id'
        })
        if (!userdata) {
            return res.status(400).json({
                message: "No user found",
                status: "0",
            });
        }

        const referncesdata = await ReferncesUsers.findOne({ totalusers: { $ne: null } }).select('totalusers')
        let transactions = {
            // ...userdata?._doc,
            investment_amount: userdata?.investment_amount,
            purchase_amount: userdata?.purchase_amount,
            credit_amount: userdata?.credit_amount,
            referal_code: userdata?.referal_code,
            referal_amount: userdata?.referal_amount,
            vendor: userdata?.vendor,
            username: userdata?.user_id?.username,
            total_slots: 5000 - Number(referncesdata?.totalusers) > 0 ? 5000 - Number(referncesdata?.totalusers) : 0,
        }






        return res.status(200).json({
            transactions,
            status: "1",
        });






    } catch (error) {
        console.log(error);
        return res.status(400).json({
            message: "Error :/", error,
            status: "0",
        });

    }
};
const getinvestorreferalinfo = async (req, res) => {

    try {

        let userdata = await Investor.findOne({
            user_id: req.body.user_id
        }).populate({
            path: 'user_id',
            select: 'username _id'
        }).populate({
            path: 'referedusers.user_id',
            select: 'username _id'
        })
        if (!userdata) {
            return res.status(400).json({
                message: "No user found",
                status: "0",
            });
        }
        let obj = []
        const referncesdata = await ReferncesUsers.findOne({ totalusers: { $ne: null } }).select('totalusers')
        for (const data of userdata?.referedusers) {
            obj.push({
                username: data?.user_id?.username,
                user_id: data?.user_id?.id,
                date: data?.date,
                amount: data?.amount,
            })
        }

        let transactions = {
            // ...userdata?._doc,
            referedusers: obj,
            total_slots: 5000 - Number(referncesdata?.totalusers) > 0 ? 5000 - Number(referncesdata?.totalusers) : 0,
            referal_code: userdata?.referal_code,
            referal_amount: userdata?.referal_amount,
            vendor: userdata?.vendor,
            username: userdata?.user_id?.username,
        }



        return res.status(200).json({
            transactions,
            status: "1",
        });






    } catch (error) {
        console.log(error);
        return res.status(400).json({
            message: "Error :/", error,
            status: "0",
        });

    }
};


///////////vendor and code senariooo implementation 

const addvendor = async (req, res) => {

    try {
        if (!req.body.user_id) {
            return res.status(400).json({
                message: "user_id required ",
                status: "0",
            });
        }
        const userdata = await Investor.findOne({
            user_id: req.body.user_id
        });
        if (!userdata) {
            return res.status(400).json({
                message: "No user found",
                status: "0",
            });
        }

        const updateuser = await Investor.updateOne(
            {
                user_id: req.body.user_id
            },
            {
                $set: {
                    vendor_status: req.body?.vendor_status,
                },
            }
        )
            .then(async (result) => {


                return res.status(200).json({
                    message: "Congratulations on becoming a Vendor",
                    status: "1",
                });
            })
            .catch((error) => {
                console.log("error", error);
                return res.status(400).json({
                    message: "Updating failed",
                    status: "0",
                });
            });








    } catch (error) {
        console.log(error);
        return res.status(400).json({
            message: "Error :/", error,
            status: "0",
        });

    }
};

function generateRandomCode(length) {
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result = '';
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return result;
}

// Example: Generate a random code of length 8
const randomCode = generateRandomCode(8);
console.log(randomCode);
const codegeneratingapi = async (req, res) => {

    try {


        const findinvestor = await Investor.findOne({ user_id: req.body.user_id, }).select('credit_amount codepurchased_amount')


        if (!findinvestor) {
            return res.status(400).json({ message: "User not exist", status: "0", });
        } if (findinvestor?.credit_amount < +req.body.amount) {
            return res.status(400).json({ message: "Amount is greater than your Credits Available", status: "0", });
        }

        let UserData = new CreditCodes({
            investor_id: req.body.user_id,
            usedBy_id: req.body.usedBy_id,
            amount: +req.body.amount,
            code: generateRandomCode(8),


        });


        await UserData.save()
            .then(async (result) => {
                console.log("result", result);
             
                let purchaseamount = findinvestor?.codepurchased_amount ? Number(req.body.amount) + Number(findinvestor?.codepurchased_amount) : Number(req.body.amount)
                let totalcredit = findinvestor?.credit_amount- Number(req.body.amount)>0 ?findinvestor?.credit_amount- Number(req.body.amount) : 0
               
                const updateuser = await Investor.updateOne(
                    { user_id: req.body.user_id, },
                    { $set: {
                        codepurchased_amount:purchaseamount,
                        credit_amount:totalcredit,
                    }, $push:{
                        codes:result._id
                    }}
                ).then(async (result) => {
                    return res.status(200).json({
                        message: "Code generated Successfully",
                        status: "1",
                    });


                })
                    .catch(error => {
                        console.log("error", error);
                        return res.status(200).json({
                            message: "Code generating failed",
                            status: "0",
                        });
                    })


            })
            .catch(error => {
                console.log("error", error);
                return res.status(200).json({
                    message: "Transaction failed",
                    status: "0",
                });
            });
    }






    catch (error) {
        console.log(error);
        return res.status(400).json({
            message: "Error :/", error,
            status: "0",
        });

    }
};
const getvendorcodes = async (req, res) => {

    try {

        let userdata = await Investor.findOne({
            user_id: req.body.user_id
        })
        if (!userdata) {
            return res.status(400).json({ message: "User not exist", status: "0", });
        } 
        let obj = []
        const referncesdata = await CreditCodes.find({  investor_id: req.body.user_id }).populate({
            path: 'usedBy_id',
            select: 'username _id'
        })
        let totalcredits=0
        for (const data of referncesdata) {
            obj.push({
                username: data?.usedBy_id?data?.usedBy_id?.username:null,
                userid: data?.usedBy_id?data?.usedBy_id?.id:null,
        
                code: data?.code,
                date: data?.createdAt,
                status: data?.status,
                amount: data?.amount,
            })
            if( data?.status == "Used"){

                totalcredits +=  +data?.amount
            }
        }

        let AllCodes = {
            // ...userdata?._doc,
            vendor_status: userdata?.vendor_status,
            credit_available: userdata?.credit_amount,
            codepurchased: userdata?.codepurchased_amount,
            code_used: totalcredits,
            Codes: obj,
        }



        return res.status(200).json({
            AllCodes,
            status: "1",
        });






    } catch (error) {
        console.log(error);
        return res.status(400).json({
            message: "Error :/", error,
            status: "0",
        });

    }
};

module.exports = { registerinvestor, getcodeamount,codegeneratingapi, getvendorcodes,getinvestordashboard, getinvestorreferalinfo, getusertraqnsactions, addvendor, referaluserapi, investorprofileupt, investortransactionsadd, getsingleinvestorprofile };




