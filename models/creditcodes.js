const { timeStamp } = require('console');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;



const schema = new Schema({
    investor_id: { type: Schema.Types.ObjectId, ref: 'User' },
    usedBy_id: { type: Schema.Types.ObjectId, ref: 'User' },
    amount: { type: String,default: null},
    code: { type: String,default: null},
    status: { type: String, enum: ["Used","New",''], default: 'New' },
   
    confirmation: { type: Boolean,default:false },
}, { timestamps: true });

schema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) {
        delete ret._id;
    }
});

module.exports = mongoose.model('CreditCodes', schema);

