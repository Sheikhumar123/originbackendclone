const { timeStamp } = require('console');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;



const schema = new Schema({
    user_id: { type: Schema.Types.ObjectId, ref: 'User' },
    transaction_ids: [{ type: Schema.Types.ObjectId, ref: 'Investortransaction' }],
    codes: [{ type: Schema.Types.ObjectId, ref: 'CreditCodes' }],
    investment_amount: { type: Number,default: null},
    purchase_amount: { type: Number,default: null},
    referal_amount: { type: Number,default: null},
   withdrawl_pending: { type: Number,default: null},
   withdrawl_amount: { type: Number,default: null},
    referedusers: [{
        amount:  { type: Number,default: null},
        date:  { type: Date,default:  Date.now},
        user_id: { type: Schema.Types.ObjectId, ref: 'User' },
    }],
    credit_amount: { type: Number,default: null},
    codepurchased_amount: { type: Number,default: null},
    referal_code: { type: String,default: null},
    isreference: { type: String,default: null},// from whom it is refered
    vendor_status: { type: Boolean,default: false},
    //payment_mode: { type: String, enum: ["UPI","Cash","Credit","Crypto",''], default: '' },
  
    status: { type: String, enum: ['Active', 'Inactive'], default: 'Active' },
}, { timestamps: true });

schema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) {
        delete ret._id;
    }
});

module.exports = mongoose.model('Investor', schema);

