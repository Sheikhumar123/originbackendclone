const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    totalusers: {
        type: Number,
        default: null,
        validate: {
            validator: function(value) {
                // Check if the totalusers value is within the limit of 5000
                return value === null || (value >= 0 && value <= 5000);
            },
            message: props => `${props.value} is not a valid total users count. Must be between 0 and 5000.`
        }
    },
}, { timestamps: true });

schema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) {
        delete ret._id;
    }
});

module.exports = mongoose.model('ReferncesUsers', schema);
