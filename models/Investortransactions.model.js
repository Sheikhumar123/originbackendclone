const { timeStamp } = require('console');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;



const schema = new Schema({
    user_id: { type: Schema.Types.ObjectId, ref: 'User' },
    amount: { type: String,default: null},
    total_amount: { type: String,default: null},
    transaction_ss: { type: String,default: null},
    code: { type: String,default: null},
    fee: { type: String,default: null},
    payment_method: { type: String, enum: ["UPI","Cash","Credit","Crypto",''], default: '' },
    type: { type: String, enum: ["Credit","Investment",''], default: '' },
    status: { type: String, enum: ["Received","Pending",'Decline'], default: 'Pending' },
   
    confirmation: { type: Boolean,default:false },
}, { timestamps: true });

schema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) {
        delete ret._id;
    }
});

module.exports = mongoose.model('Investortransaction', schema);

